import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';

import App from './components/App';
import Question from './components/Question';
import Layout from './components/Layout';
import Result from './components/Result';
import './index.css';
import store from './store';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

ReactDOM.render(
  <Provider store={ store }>
    <MuiThemeProvider>
      <Router>
        <Switch>
          <Layout>
            <Route exact path="/" component={ App } />
            <Route exact path="/questions/:order" component={ Question } />
            <Route exact path="/result" component={ Result } />
          </Layout>
        </Switch>
      </Router>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root')
);
